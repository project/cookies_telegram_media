# COOKiES Telegram Media

Cookie consent integration for https://www.drupal.org/project/cookies module
for the https://www.drupal.org/project/telegram_media_type module.

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainer


## Requirements

This module requires the following modules:
- [cookies](https://www.drupal.org/project/cookies)
- [telegram_media_type](https://www.drupal.org/project/telegram_media_type)


## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Navigate to Administration > Extend and enable the module.


## Maintainer

- Robert Kasza - [kaszarobert](https://www.drupal.org/u/kaszarobert)
