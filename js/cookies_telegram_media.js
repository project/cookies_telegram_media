/**
 * @file
 * Defines Javascript behaviors for the cookies module.
 */
(function (Drupal, $) {
  'use strict';

  /**
   * Define defaults.
   */
  Drupal.behaviors.cookiesTelegram = {
    initTelegram: {},

    activate: function (context) {
      $('script[data-sid="telegram"]').each(function () {
        var replacement = $(this).clone().removeAttr('type').removeAttr('data-sid');
        $(this).replaceWith(replacement.prop('outerHTML'));
      });
    },

    fallback: function (context) {
      $('.telegram-embedded-content').cookiesOverlay('telegram');
    },

    attach: function (context) {
      var self = this;
      if (Drupal.behaviors.hasOwnProperty('telegramMediaEntity')) {
        // Take over the init function and remove it from the original context.
        if (typeof Drupal.behaviors.telegramMediaEntity.attach === 'function') {
          self.initTelegram['attach'] = Drupal.behaviors.telegramMediaEntity.attach;
          self.initTelegram.attach.bind(self.initTelegram);
          Drupal.behaviors.telegramMediaEntity.attach = null;
        }
      }

      document.addEventListener('cookiesjsrUserConsent', function (event) {
        var service = (typeof event.detail.services === 'object') ? event.detail.services : {};
        if (typeof service.telegram !== 'undefined' && service.telegram) {
          self.activate(context);
        } else {
          self.fallback(context);
        }
      });
    }
  };
})(Drupal, jQuery);
